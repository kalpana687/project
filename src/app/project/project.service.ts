import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class cartItem {
  availabletime: string;
  category: string;
  description: string;
  name : string;
  price : number;
  vegflag : string;
  length : 100;
}

export class ProjectService {

  constructor(public httpClient : HttpClient) { }

  getDetail() :Observable<cartItem[]>{
    let url = environment.apiUrl + 'menu.json';
    return this.httpClient.get<cartItem[]>(url);
 }

 
}
