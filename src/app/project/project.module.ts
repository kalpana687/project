import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectRoutingModule } from './project-routing.module';
import { ProjectComponent } from './project.component';
import { ProjectService } from './project.service';
import { HttpClientModule } from '@angular/common/http';
import {NgPipesModule} from 'ngx-pipes';

@NgModule({
  declarations: [ProjectComponent],
  imports: [
    CommonModule,
    ProjectRoutingModule,
    HttpClientModule,
    NgPipesModule
  ],
  providers: [ProjectService]
})
export class ProjectModule { }
