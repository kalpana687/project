import { Component, OnInit } from '@angular/core';
import { ProjectService, cartItem } from './project.service'

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {
  public menuItems : Array<cartItem> = [];
  public cartArray : Array<any> = [];
  public activeMenuLink :string = 'Combos';
  constructor(public projectService : ProjectService) { }

  ngOnInit() {
    this.projectService.getDetail().subscribe((data: cartItem[]) => {
      this.menuItems = data;
      
    });
  }

  addCart(item){
    this.cartArray.push(item);
    
  }

  activeMenu(active){
    this.activeMenuLink = active;

  }

  clearCart(){
    this.cartArray = [];
  }

  removeCart(item){
    this.cartArray.splice(item, 1)
  }

}
