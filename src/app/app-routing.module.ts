import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectComponent } from './project/project.component'

const routes: Routes = [
  { path: '', component: ProjectComponent, pathMatch: 'full' },
  { path: '', loadChildren: () => import('./project/project.module').then(m => m.ProjectModule) },
  { path: '**', component: ProjectComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
