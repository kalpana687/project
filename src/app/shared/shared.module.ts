import { NgModule , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    CommonModule,
    MDBBootstrapModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [HeaderComponent]
})
export class SharedModule { }
 